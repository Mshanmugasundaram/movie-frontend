import React, { Component } from 'react';
import { Button, Grid, TextField } from '@material-ui/core';
import axios from 'axios';
import {withRouter, Redirect, Router, Route, NavLink } from 'react-router-dom'
import Header from '../Header/header'
import history from '../../history'


class ForgetPassword extends Component {
    state = {
        email: '',
        username: '',
        password: ''
    }

    forgetPassword = () => {
        console.log(this.state)
        axios.put('http://localhost:7000/user/forgetpassword', {
            email: this.state.email,
            username: this.state.username,
            newPassword: this.state.password
        }). then(response => {
            console.log(response)
            alert("Password updated successfully")
            this.props.history.push('/')
        }). catch(err =>{
            alert("Enter valid email and username")
        })
    }

    render() {
        return (
            <Grid container direction='column'>
              <Grid item>
                <Header />
              </Grid>
            <div style={{
                textAlign: 'center',
                marginTop: '50px',}}>
              <h1 style={{
                fontWeight: 400,
                fontFamily: 'sans-serif',
                color: 'blue',
                letterSpacing: '2px',
              }}>Forget Password</h1>
              <Grid item>
              <TextField style={{
                marginBottom: '20px',
                width: '25%',
                }}
                label='Email'
                variant = 'outlined'
                onChange={(e) => this.setState({email: e.target.value})}
                />
                </Grid>
                <Grid item>
                <TextField style={{
                marginBottom: '20px',
                width: '25%',
                }}
                label='Username'
                variant = 'outlined'
                onChange={(e) => this.setState({username: e.target.value})}
                />
                </Grid>
                <Grid item>
                <TextField style={{
                marginBottom: '20px',
                width: '25%',
                }}
                label='Password'
                variant = 'outlined'
                onChange={(e) => this.setState({password: e.target.value})}
                />
                </Grid>
                <Grid item>
                <Button style={{
                    marginTop: '15px',
                    marginLeft: '20px'
                }} variant='contained'
                color='primary'
                onClick = {() => this.forgetPassword()}
                >Forget Password</Button>
                </Grid>
              </div>
              </Grid>


        )
    }
}

export default withRouter(ForgetPassword)