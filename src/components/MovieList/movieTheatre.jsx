import React, {Component} from 'react';
import {Button, Grid, TextField} from '@material-ui/core';
import axios from 'axios';
import {withRouter, Redirect, Router, Route, NavLink } from 'react-router-dom'
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import dropDown from 'react-dropdown'
import Header from '../Header/header'


class MovieTheatre extends Component {
    state = {
        selectedTheatre: '',
        selectedMovie: '',
        seat: '',
        reserved: [],
        theatres: [],
        movies: []
    }


    ownerTheatre = (id) => {
        axios.get(`http://localhost:7000/theatre/owner/${id}`, {
          headers: {
            Authorization: `JWT ${this.state.token}`
          }
        }).then(response => {
          this.setState({theatres: response.data.data})
          console.log(this.state.theatre)
        })
      }

    listMovie = () => {
        axios.get("http://localhost:7000/movie",{
          headers: {
            Authorization: `JWT ${this.state.token}`
          }
        })
        .then((movies) => {
            let data = movies.data.data
            this.setState({movies: data})
        })
    }

    theatreHandleChange = (e) => {
        console.log(e.target.value)
        this.setState({selectedTheatre: e.target.value})
      }
    
      movieHandleChange = (e) => {
        console.log(e.target.value)
        this.setState({selectedMovie: e.target.value})
      }

      addMovieTheatre = () => {
        axios.post(`http://localhost:7000/movie/add/theatre`, {
          name: this.state.selectedMovie,
          theatreName: this.state.selectedTheatre,
          seat: this.state.seat,
          reserved: this.state.reserved
        }).then(response => {

          console.log(response.status)
          if(response.status == 204) {
            alert("Movie already assigned to this Theatre. No of seats only updated")
            this.props.history.push('/movies')
          } else {
            alert('Success')
            this.props.history.push('/movies')
          }
      }) .catch(err => {
        alert(err)
      })
      }

    async componentDidMount() {
        let token = await localStorage.getItem('token')
        if(localStorage.getItem('token')) {
          await this.setState({token});
          let id = await localStorage.getItem('id')
          this.ownerTheatre(id);
          this.listMovie()
        } else{
          this.props.history.push('/')
        }
      }

    add = () => {
        console.log(this.state.reserved)
    }

    reserved = (e) => {
        let i = 0
        // let reserved =  [...this.state.reserved]
        // reserved[i++] = e.target.value
        this.setState({reserved: [...this.state.reserved, e.target.value]})
    }

    render () {
        return (
            <Grid container direction='column'>
                <Grid item>
                    <Header />
                </Grid>
                <div style={{
                textAlign: 'center',
                marginTop: '50px',}}>
              <h1 style={{
                fontWeight: 400,
                fontFamily: 'sans-serif',
                color: 'blue',
                letterSpacing: '2px',
              }}>Theatres</h1>
                <Grid item>
                    <FormControl style={{ width:'25%', textAlign: 'center', marginBottom: 15}}>
                        <InputLabel id="demo-simple-select-label">Select Theatre</InputLabel>
                        <Select
                            labelId="demo-simple-select-label"
                            id="demo-simple-select"
                            value={this.state.selectedTheatre}
                            onChange={(e) => this.theatreHandleChange(e)}
                        >
                            { this.state.theatres.map((theatre) => {
                                return <MenuItem value={theatre.name}>{theatre.name}</MenuItem>
                            })
                    }
                </Select>
                </FormControl>
                </Grid>
                <Grid item>
                    <FormControl style={{ width:'25%', textAlign: 'center', marginBottom: 15}}>
                        <InputLabel id="demo-simple-select-label">Select Movies</InputLabel>
                        <Select
                            labelId="demo-simple-select-label"
                            id="demo-simple-select"
                            value={this.state.selectedMovie}
                            onChange={(e) => this.movieHandleChange(e)}
                        >
                            { this.state.movies.map((movie) => {
                                return <MenuItem value={movie.name}>{movie.name}</MenuItem>
                            })
                    }
                </Select>
                </FormControl>
                </Grid>
                <Grid>
                <TextField style={{
                    marginBottom: '20px',
                    width: '25%',
                }} 
                variant='standard'
                label='No of Seats'
                onChange={(e) => this.setState({ seat: e.target.value})}
                />
                </Grid>
                {/* <Grid item>
                <TextField style={{
                    marginBottom: '20px',
                    width: '25%',
                }} 
                variant='standard'
                label='Reserved Seats'
                onChange={(e) => this.setState({reserved: e.target.value})}
                />
                </Grid> */}

                <Grid item>
                  <Button style={{textTransform: 'capitalize', backgroundColor: 'blue', color: 'white'}} onClick={()=> {this.addMovieTheatre()}}>Add</Button>
                </Grid>
                </div>
            </Grid>
        )
    }
}

export default withRouter(MovieTheatre)