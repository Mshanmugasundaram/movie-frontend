import React, {Component} from 'react';
import { AppBar, Toolbar, Typography, Button, Grid, TextField} from '@material-ui/core';
import {createMuiTheme, ThemeProvider} from '@material-ui/core/styles';
import { makeStyles } from "@material-ui/core";
import blue from '@material-ui/core/colors/blue'
import axios from 'axios';
import {withRouter, Redirect, Router, Route, NavLink } from 'react-router-dom'
import Header from '../Header/header'


class AddTheatre extends Component {
    state= {
        
    }

    newMovie = () => {
        axios.post("http://localhost:7000/theatre/add", {
            name: this.state.name,
            location: this.state.location,
            ownerId: localStorage.getItem('id')
        }).then(response => {
            console.log(response)
            alert('Theatre added Successfully')
            this.props.history.push('/movies')
        }) .catch(err => {
          alert(err)
        })
    }

    ownerTheatre = (id) => {
      axios.get(`http://localhost:7000/theatre/owner/${id}`, {
        headers: {
          Authorization: `JWT ${this.state.token}`
        }
      }).then(response => {
        this.setState({theatre: response.data.data})
        console.log(this.state.theatre)
      })
    }
    async componentDidMount() {
      let token = await localStorage.getItem('token')
      if(localStorage.getItem('token')) {
        await this.setState({token});
        let id = await localStorage.getItem('id')
        // this.ownerTheatre(id);
      } else{
        this.props.history.push('/')

      }
    }

    render() {
        return (
          <Grid container direction='column'>
          <Grid item>
            <Header />
          </Grid>
            <div style={{
                textAlign: 'center',
                marginTop: '50px',}}>
              <h1 style={{
                fontWeight: 400,
                fontFamily: 'sans-serif',
                color: 'blue',
                // textTransform: 'uppercase',
                letterSpacing: '2px',
              }}>Add Theatre</h1>
              <Grid item>

              <TextField style={{
                    marginBottom: '20px',
                    width: '25%',
                }} 
                variant='standard'
                label='Theatre Name'
                onChange={(e) => this.setState({ name: e.target.value})}
                />
              </Grid>
              <Grid item>

                <TextField style={{
                    marginBottom: '20px',
                    width: '25%',
                }} 
                variant='standard'
                label='Location'
                onChange={(e) => this.setState({ location: e.target.value})}
                />
                </Grid>
              <Grid item>
                  <Button style={{textTransform: 'capitalize', backgroundColor: 'blue', color: 'white'}} onClick={()=> {this.newMovie()}}>Add</Button>
                </Grid>
              </div>
              </Grid>
        )
    }
}

export default withRouter(AddTheatre)