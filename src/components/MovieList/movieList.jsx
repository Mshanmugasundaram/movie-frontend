import React, {Component} from 'react';
import { AppBar, Toolbar, Typography, Button, Grid, TextField} from '@material-ui/core';
import {createMuiTheme, ThemeProvider} from '@material-ui/core/styles';
import { makeStyles } from "@material-ui/core";
import blue from '@material-ui/core/colors/blue'
import axios from 'axios';
import {withRouter, Redirect, Router, Route, NavLink } from 'react-router-dom'
import GridList from '@material-ui/core/GridList'
import Card from '@material-ui/core/Card';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import Header from '../Header/header'

class MovieList extends Component {
    state = {
        movies: [],
        token: '',
        available: []
    }

    getMovies = () => {
      console.log()
        axios.get("http://localhost:7000/movie",{
          headers: {
            Authorization: `JWT ${this.state.token}`
          }
        })
            .then((movies) => {

                let data = movies.data.data
                this.setState({movies: data})
            })
    } 


    async componentDidMount() {
      let token = await localStorage.getItem('token')
      if(localStorage.getItem('token')) {
        await this.setState({token});
        this.getMovies();
      } else{
        this.props.history.push('/')

      }
    }

    listTheatres =async(id) => {
      await sessionStorage.setItem('selectedMovieId', id)
      console.log(id)
      this.props.history.push('/theatre')
    }

    newMovies = () => {
      this.props.history.push('/addMovies')
    }

    addTheatre = () => {
      this.props.history.push('/addTheatre')
    }
    
    yourTickets = () => {
      this.props.history.push({
        pathname: '/tickets',})
      }

    addMovies = () => {
      this.props.history.push('/movie/theatre')
    }
    
    
    logout = () => {
      alert('DO you want to logout..??')
      localStorage.clear()
      this.props.history.push('/')
    }

    render() {
        const movies = this.state.movies;
        const moviesLength = movies.length;
        return(
          <Grid container direction='column'>
          <Grid item>
            <Header />
          </Grid>
          <div style={{
            
            textAlign: 'center',
            marginTop: '30px',}}>
          <Typography variant='h4' style={{fontFamily: 'sans-serif', textTransform: 'capitalize', letterSpacing: 2, marginBottom: '30px'}}>Welcome {localStorage.getItem('username')}..!!</Typography>
          <hr/>
          <h1 style={{
            fontWeight: 400,
            fontFamily: 'sans-serif',
            color: 'blue',
            // textTransform: 'uppercase',
            letterSpacing: '2px',
            display: 'inline-block'
          }}>Movies</h1>
          <Button variant='outlined' style={{float: 'right', marginRight: 100, marginTop: 30}}color='primary' onClick={() => this.logout()}>logout</Button>
          <Button variant='outlined' style={{float: 'right', marginRight: 100, marginTop: 30}} color='primary' onClick={() => this.yourTickets()}>Your Tickets</Button>
          {localStorage.getItem('role')=='owner' ? <Button variant='outlined' style={{float:'right', marginRight: 100, marginTop: 30}}color='primary' onClick={() => this.addMovies()}>Add Movies</Button> : null} 
          {localStorage.getItem('role')=='owner' ? <Button variant='outlined' style={{marginLeft: 500, marginTop: 30}}color='primary' onClick={() => this.addTheatre()}>Add Theatre</Button> : null} 
          {localStorage.getItem('role')=='admin' ? <Button variant='outlined' style={{marginLeft: 500, marginTop: 10}}color='primary' onClick={() => this.newMovies()}>New Movies</Button> : null} 
          <hr/>
          <Grid container>
            <Grid item lg={12}>
            {moviesLength != 0 ? movies.map(movie => {
              return (
              <Card style={{display: 'inline-block', marginLeft: 10, marginTop: 20, maxWidth: 200, height:300}}>
              <CardActionArea>
                <CardMedia
                  component="img"
                  alt={movie.name}
                  style={{height: 150, width: 195,}}
                  image={movie.movieImage ? (`http://localhost:7000/${movie.movieImage}`) : ''}
                  title={movie.name}
                />
                <CardContent >
                  <Typography style={{textTransform: 'uppercase'}} variant="subtitle1" component="h2">
                    {movie.name}
                  </Typography>
                  <Typography variant="body2" color="textSecondary" component="p">
                    {movie.description ? movie.description : " "}
                  </Typography>
                </CardContent>
              </CardActionArea>
              <CardActions>
                <Button 
                  style={{textAlign: "center"}}
                  size="small" 
                  color="primary" 
                  fullWidth
                  align='justify'
                  onClick={() =>{
                    this.listTheatres(movie._id)}
                  }
                  > Book Now
                </Button>
              </CardActions>
            </Card>
            );
          }): 
          <Grid>
            <h2 style={{fontFamily: 'sans-serif', letterSpacing: 2, fontWeight: 200}}>No Movies Found</h2>
            <hr/>
          </Grid> 
          }
          </Grid>
          </Grid>
          </div>
          </Grid>
        )
      }
    }




export default withRouter(MovieList);