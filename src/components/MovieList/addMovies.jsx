import React, {Component, useState} from 'react';
import { AppBar, Toolbar, Typography, Button, Grid, TextField} from '@material-ui/core';
import {createMuiTheme, ThemeProvider} from '@material-ui/core/styles';
import { makeStyles } from "@material-ui/core";
import blue from '@material-ui/core/colors/blue'
import axios from 'axios';
import {withRouter, Redirect, Router, Route, NavLink } from 'react-router-dom'
import Header from '../Header/header'
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormHelperText from '@material-ui/core/FormHelperText';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import dropDown from 'react-dropdown'

class AddMovies extends Component {

    state= {
      selectedFile: '',
      theatres:[]
    }
     

    fileSelectedHandler = (event) => {
      console.log(event.target)
      this.setState({
        selectedFile: event.target.files[0]
      })
      }

    newMovie = () => {
      const formData = new FormData()
      formData.append('movieImage', this.state.selectedFile)
      formData.append('name', this.state.name)
      formData.append('description', this.state.description)
      axios.post("http://localhost:7000/movie/add", formData
    )
      .then(response => {

          alert('Movie added Successfully')
          this.props.history.push('/movies')
      }) .catch(err => {
        console.log(err)
        alert(err)
      })
    }

    ownerTheatre = (id) => {
      axios.get(`http://localhost:7000/theatre/owner/${id}`, {
        headers: {
          Authorization: `JWT ${this.state.token}`
        }
      }).then(response => {
        this.setState({theatres: response.data.data})
        console.log(this.state.theatres)
      })
    }
    async componentDidMount() {
      let token = await localStorage.getItem('token')
      if(localStorage.getItem('token')) {
        await this.setState({token});
        let id = await localStorage.getItem('id')
        this.ownerTheatre(id);
      } else{
        this.props.history.push('/')

      }
    }

    handleChange = (e) => {
      console.log(e.target.value)
      this.setState({selectedTheatre: e.target.value})
    }

    render() {
        return (
            <Grid container direction='column'>
              <Grid item>
                <Header />
              </Grid>
            <div style={{
                textAlign: 'center',
                marginTop: '50px',}}>
              <h1 style={{
                fontWeight: 400,
                fontFamily: 'sans-serif',
                color: 'blue',
                letterSpacing: '2px',
              }}>Theatres</h1>
              <Grid item>
                <TextField style={{
                    marginBottom: '20px',
                    width: '25%',
                }} 
                variant='standard'
                label='Movie Name'
                onChange={(e) => this.setState({ name: e.target.value})}
                />
                </Grid>
              <Grid item>
                <TextField style={{
                    marginBottom: '20px',
                    width: '25%',
                }} 
                variant='standard'
                label='Description'
                onChange={(e) => this.setState({ description: e.target.value})}
                />
              </Grid>
              <Grid item>
              <input type='file' id='file' onChange={(e) => this.fileSelectedHandler(e)}
              />
              </Grid>
              <Grid item>
                  <Button style={{textTransform: 'capitalize', backgroundColor: 'blue', color: 'white', marginTop: 30}} onClick={()=> {this.newMovie()}}>Add</Button>
                </Grid>
              </div> 
              </Grid>
              )
            }
}

export default withRouter(AddMovies)