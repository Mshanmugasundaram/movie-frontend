import React, {Component} from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import {withRouter, Redirect, Router, Route, NavLink } from 'react-router-dom'
import { AppBar, Toolbar, Typography, Button, Grid, TextField} from '@material-ui/core';
import Header from '../Header/header'
import axios from 'axios'

class Tickets extends Component {
  state = {
    token: '',
    id: '',
    ticketsDetails: [],
    seats: []
  }
  goBack = () => {
    this.props.history.push('/movies')
  }

  yourTickets = () => {
    console.log('function')
    console.log(this.state.token)
    axios.get(`http://localhost:7000/booked`, {
      headers: {
        Authorization: `JWT ${this.state.token}`
      },
        userId: localStorage.getItem('id'),
      })
        .then(response => {
          console.log(response)
          this.setState({ticketsDetails: response.data.data})
          console.log("->",this.state.ticketsDetails)
          {this.state.ticketsDetails.seats.map(seat => {
            console.log(seat)
          })}
          let tickets = sessionStorage.setItem('tickets', response.data.data)

        })
      }

  async componentDidMount() {
    let token = await localStorage.getItem('token');
    this.setState({token: token})
    let id = await localStorage.getItem('id');
    this.setState({id: id})
     this.yourTickets();

  }
  
  async componentWillMount() {
  }

  render () {
    let count = 0
    return (
    <Grid container direction='column'>
      <Grid item>
            <Header />
        </Grid>
        <h1 style={{
          fontWeight: 400,
          fontFamily: 'sans-serif',
          color: 'blue',
          // textTransform: 'uppercase',
          letterSpacing: '2px',
          textAlign: 'center',
          marginTop: 50,
        }}>Your Tickets</h1>
        <div style={{
            textAlign: 'center',
            marginTop: '10px',
            marginLeft: '400px'}}>
        <Grid item>
          <TableContainer>
          <Table style={{maxWidth: 650}}aria-label="simple table">
              <TableHead>
                <TableRow>
                  <TableCell align="right">Movie</TableCell>
                <TableCell align="right">Theatre</TableCell>
                  <TableCell align="right">Seat No</TableCell>
                </TableRow>
              </TableHead>
              <TableBody>
                {this.state.ticketsDetails.seats ? this.state.ticketsDetails.seats.map((seat) => (
                  <TableRow key={seat}>
                    <TableCell component="th" scope="row" align='right'>
                      {this.state.ticketsDetails.movie[count]}
                    </TableCell>
                    <TableCell align='right'>
                      {this.state.ticketsDetails.theatre[count]}
                    </TableCell>
                    <TableCell align='right'>
                      {seat}
                    </TableCell>
                    <div style={{display: "none"}}>{count = count + 1}</div>
                  </TableRow>
                )) : null}
              </TableBody>
            </Table>
          </TableContainer>
        </Grid>
        <Grid item>
          <Button style={{marginTop: 20}}variant='outlined' color='primary' onClick={()=> this.goBack()}>Go Back</Button>
        </Grid>
      </div>
     </Grid>
  )}
}


export default withRouter(Tickets)