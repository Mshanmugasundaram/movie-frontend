import React, { Component } from 'react';
import { Button, Grid, TextField } from '@material-ui/core';
import axios from 'axios';
import {withRouter, Redirect, Router, Route, NavLink } from 'react-router-dom'
import Header from '../Header/header'
import history from '../../history'

class Login extends Component {
    state = {
        email: '',
        password:''
    }

    login = () => {
        let data = this.state
        axios.post('http://localhost:7000/user', {
            email: data.email,  
            password: data.password
        }).then((response) => {
            let data = response.data;
            localStorage.setItem('token', data.token);
            localStorage.setItem('role', data.role);
            localStorage.setItem('id', data._id);
            localStorage.setItem('username', data.username)
            localStorage.setItem('role', data.role);
            localStorage.setItem('email', data.email);
            if(data) {
                this.props.history.push({
                    pathname: '/movies',
                    user: data
                })
            }
        }).catch(err => {
            alert('Invalid Credentials..')
        })
    }

    forgetPassword = () => {
        this.props.history.push("/forget/password")
    }

    async componentDidMount() {
        if(localStorage.getItem('token')) {
            this.props.history.push('/movies')
        }
    }

    render() {
        return (
            <Grid container direction='column'>
                <Header />
            <div style={{
                textAlign: 'center',
                marginTop: '50px',
            }}>
            <h1 style={{
                fontWeight: 400,
                fontFamily: 'sans-serif',
                color: 'blue',
                // textTransform: 'uppercase',
                letterSpacing: '2px',
            }}>Login</h1>
            <Grid item>
                <TextField style={{
                    marginBottom: '20px',
                    width: '25%',
                }} 
                variant='outlined'
                label='Email'
                autoComplete
                onChange={(e) => this.setState({ email: e.target.value})}
                />
            </Grid>
            <Grid item>
                <TextField style={{
                    marginBottom: '25px',
                    width: '25%',
                }}
                variant='outlined'
                type='password'
                label='Password'
                onChange={(e) => this.setState({ password: e.target.value})}

                />
            </Grid>
            <Grid item>
                <a href='' onClick={() => this.forgetPassword()}>Forget Password..!</a>
            </Grid>
            <Grid item>
                <Button style={{
                    marginTop: '25px',
                    marginRight: '10px',
                }}
                variant='outlined'
                color='primary'
                onClick = {() => {
                    this.login()
                }}
                >Login</Button>
                <Button style={{
                    marginTop: '25px',
                    marginLeft: '20px'
                }} variant='outlined'
                color='primary'
                onClick = {() => this.props.history.push('/signup')}
                >Signup</Button>
            </Grid>
            </div>
        </Grid>
        )
    }
}

export default withRouter(Login);