import React, {Component} from 'react';
import { AppBar, Toolbar, Typography, Button, Grid, TextField} from '@material-ui/core';
import {createMuiTheme, ThemeProvider} from '@material-ui/core/styles';
import { makeStyles } from "@material-ui/core";
import blue from '@material-ui/core/colors/blue'
import axios from 'axios';
import {withRouter, Redirect, Router, Route, NavLink } from 'react-router-dom'
import GridList from '@material-ui/core/GridList'
import Card from '@material-ui/core/Card';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import Header from '../Header/header'

class ListTheatre extends Component {
    state = {
        theatres: [],
        token: ''
    }
    
    listTheatres = (id) => {
      axios.get(`http://localhost:7000/theatre/${id}`, {
        headers: {
          Authorization: `JWT ${this.state.token}`
        }
      })
      .then((response) => {
          let data = response.data.data
          this.setState({theatres: data})
          localStorage.setItem('theatre', Array(data))

      }).catch(err => {
        alert(err)
      })

    }

    getSeats = async(theatreId, movie) => {
      await sessionStorage.setItem('theatreId', theatreId)
      this.props.history.push({pathname:'/seat'})

    }
    async componentDidMount() {
      console.log('theatre')
      console.log(localStorage.getItem('theatre'))
      console.log(this.props.location.theatres)
      
      if(localStorage.getItem('token')) {
        let token = await localStorage.getItem('token')
        console.log(token)
        await this.setState({token});
        let id = await sessionStorage.getItem('selectedMovieId')
        this.listTheatres(id)
      } else {
        console.log('else')
        this.props.history.push('/')
      }
      
  }
    render() {
      let id = sessionStorage.getItem('selectedMovieId')
        return(

          <Grid container direction='column'>
            <Grid item>
              <Header/>
            </Grid>
            <div style={{
              textAlign: 'center',
              marginTop: '50px',}}>
            <h1 style={{
              fontWeight: 400,
              fontFamily: 'sans-serif',
              color: 'blue',
              // textTransform: 'uppercase',
              letterSpacing: '2px',
            }}>Theatres</h1>
            <hr/>
            <Grid container>
              <Grid item lg={12}>
              {this.state.theatres.map(theatre => {
                return (
                <Card style={{display: 'inline-block', marginLeft: 10, marginTop: 20, maxWidth: 200, height:300}}>
                <CardActionArea>
                  <CardMedia
                    component="img"
                    alt='Theatre Logo'
                    style={{height: 150, width: 195,}}
                    image={(`http://localhost:7000/uploads\\theatre.jpg`)}
                    title= 'Theatre Logo'
                  />
                  <CardContent >
                    <Typography style={{textTransform: 'capitalize'}} variant="h6" component="h2">
                      {theatre.name}
                    </Typography>
                    <Typography variant="body2" color="textSecondary" component="p">
                      {theatre.location}
                    </Typography>
                  </CardContent>
                </CardActionArea>
                <CardActions>
                  <Button 
                    style={{textAlign: "center", alignItems: 'center'}}
                    size="small" 
                    color="primary" 
                    fullWidth
                    onClick={() => this.getSeats(theatre._id, id)}
                    >Available Seats
                  </Button>
                </CardActions>
              </Card>
              );
            })}
            </Grid>
            </Grid>
            </div>
            </Grid>
          )
    }
}

export default withRouter(ListTheatre)