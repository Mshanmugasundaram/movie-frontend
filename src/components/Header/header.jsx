import React from 'react';
import { AppBar, Toolbar, Typography, Button, Grid} from '@material-ui/core';
import {createMuiTheme, ThemeProvider} from '@material-ui/core/styles';
import { makeStyles } from "@material-ui/core";
import blue from '@material-ui/core/colors/blue'

const useStyles = makeStyles(() => ({
    typographyStyles: {
        fontSize: '24px',
        marginLeft: '20px',
        letterSpacing: '1.8px',
        textTransform: 'uppercase'
    },
    appBar: {
        color: 'white',
    }
}))

const theme = createMuiTheme({
    palette: {
        primary: blue
    }
})

const Header = () => {
    const classes = useStyles();

    return (
        <Grid>
            <ThemeProvider theme = {theme}>
                <AppBar position = 'static' className={classes.appBar}>
                    <Toolbar>
                        <Typography className={classes.typographyStyles}>Ticketmania</Typography>
                    </Toolbar>
                </AppBar>
            </ThemeProvider>
        </Grid>
    )
}

export default Header;