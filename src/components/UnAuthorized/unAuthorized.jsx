import React, { Component } from 'react';
import { Button, Grid, TextField } from '@material-ui/core';
import axios from 'axios';
import {withRouter, Redirect, Router, Route, NavLink } from 'react-router-dom'
import Header from '../Header/header'
import history from '../../history'


class UnAuthorized extends Component {

    render() {
        return (
            <div style={{
                textAlign: 'center',
                marginTop: '50px',}}>
            <h1 style={{
                fontWeight: 400,
                fontFamily: 'sans-serif',
                color: 'blue',
                // textTransform: 'uppercase',
                letterSpacing: '2px',
            }}>Error</h1>
            <h2 style={{
                fontWeight: 300,
                fontFamily: 'sans-serif',
                color: 'blue',
                // textTransform: 'uppercase',
                letterSpacing: '2px',
            }}>Authorization Required...!!!</h2>
            
            </div>
        )
    }
}


export default withRouter(UnAuthorized);