import React, {Component} from 'react';
import {Button, Grid, TextField} from '@material-ui/core';
import axios from 'axios';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import { withRouter } from 'react-router-dom'
import Header from '../Header/header';


class Signup extends Component {
    state = {
        email: '',
        username: '',
        password:'',
        role:''
    }

    signup = () => {
        let data = this.state
        axios.post('http://localhost:7000/user/signup', {
            email: data.email,
            username: data.username,
            password: data.password,
            role: data.role
        }).then((response) => {
            let data = response.data;
            this.props.history.push('/')
        }).catch(err => {
            alert(err)
        })
    }

    roleHandleChange = (e) => {
        this.setState({role: e.target.value})
        console.log(this.state.role)
    }
     render() {
        return (
            <Grid container direction='column'>
                < Header />
            <div style={{
                textAlign: 'center',
                marginTop: '50px',
            }}>
            <h1 style={{
                fontWeight: 400,
                fontFamily: 'sans-serif',
                color: 'blue',
                // textTransform: 'uppercase',
                letterSpacing: '2px',
            }}>Sign up</h1>
            <Grid item>
                <TextField style={{
                    marginBottom: '20px',
                    width: '25%',
                }} 
                variant='outlined'
                label='Email'
                onChange={(e) => this.setState({ email: e.target.value})}
                />
            </Grid>
            <Grid item>
                <TextField style={{
                    marginBottom: '20px',
                    width: '25%',
                }}
                variant='outlined'
                label='Username'
                onChange={(e) => this.setState({ username: e.target.value})}

                />
            </Grid>
            <Grid item>
                <TextField style={{
                    marginBottom: '20px',
                    width: '25%',
                }}
                variant='standard'
                label='Password'
                onChange={(e) => this.setState({ password: e.target.value})}

                />
            </Grid>
            <Grid item>
            <FormControl style={{ width:'25%', textAlign: 'center', marginBottom: 15}}>
        <InputLabel id="demo-simple-select-label">  Role</InputLabel>
        <Select
          labelId="demo-simple-select-label"
          id="demo-simple-select"
          variant='standard'
          value={this.state.role}
          onChange={(e) => this.roleHandleChange(e)}
        >
          <MenuItem value={'user'}>User</MenuItem>
          <MenuItem value={'owner'}>Owner</MenuItem>
        </Select>
      </FormControl>
            </Grid>
            
            <Grid item>
                <Button style={{
                    marginTop: '15px',
                    marginLeft: '20px'
                }} variant='contained' color='primary' onClick={() => this.signup()}>Signup</Button>
            </Grid>
            </div>
        </Grid>
        )
    }
}

export default withRouter(Signup);