import React, {Component} from 'react';
import { Button, Grid } from '@material-ui/core';
import axios from 'axios';
import { withRouter } from 'react-router-dom'
import Header from '../Header/header'

class SeatList extends Component  {
    state = {
        token:'',
        seat: [],
        id: [],
        reserved: [],
    }

    book = (theatreId, movieId) => {
        axios.put(`http://localhost:7000/seat/${theatreId}/${movieId}`, {
        userId: localStorage.getItem('id'),
        seats: this.state.id
    }).then(response => {
            console.log(response)
            alert("Booking completed successfully")
            this.props.history.push('/movies')
        }).catch(err => {
            alert(err)
        }) 
    }

    getSeats = async(theatreId, movieId) => {
      console.log(theatreId)
        axios.get(`http://localhost:7000/seat/${theatreId}/${movieId}`,{
          headers: {
            Authorization: `JWT ${this.state.token}`
          }
        })
        .then((response) => {
            let seatList = [];
            let data = response.data.data
            console.log(data)
            for(let i=1; i<=data.seat; i++) {
              seatList.push(i)
            }
            this.setState({'seat': seatList})
            this.setState({'reserved': data.reserved})
            console.log(this.state.seat)
            console.log(this.state.reserved)
        }).catch(err => {
          alert(err)
        })
      }

      reset = async(theatreId, movieId) => {
        await axios.put(`http://localhost:7000/seat/reset/${theatreId}/${movieId}`).then(response => {
          console.log("response")
          alert("Successfully restarted")
          this.props.history.push('/movies')
        }).catch(err => {
            alert(err)
        }) 
      }

    async componentDidMount() {
        let token = await localStorage.getItem('token')
        if(localStorage.getItem('token')) {
          await this.setState({token});
          let theatreId = await sessionStorage.getItem('theatreId')
          let movieId = await sessionStorage.getItem('selectedMovieId')
          this.getSeats(theatreId, movieId)
        } else {
            this.props.history.push('/')
        }
    }

    button = (seat) => {
      this.setState({id: [...this.state.id, seat]})
    }

    render() {
      let theatreId = sessionStorage.getItem('theatreId')
      let movieId = sessionStorage.getItem('selectedMovieId')

        return (
          <Grid container direction='column'>
            <Grid item>
            <Header/>
            </Grid>
            <div style={{
                textAlign: 'center',
                marginTop: '50px',}}>
              <h1 style={{
                fontWeight: 400,
                fontFamily: 'sans-serif',
                color: 'blue',
                letterSpacing: '2px',
              }}>Seats</h1> 
              <hr/>
              <Grid item lg={14}>
                {this.props.location.seats == 0 ? <h1 style={{
                fontWeight: 400,
                fontFamily: 'allura',
                color: 'black',
                letterSpacing: '2px',
                marginBottom: 20
              }}>No Seats Available</h1> : 
              <h1 style={{
                fontWeight: 400,
                fontFamily: 'allura',
                color: 'black',
                letterSpacing: '2px',
                marginBottom: 30
              }}>Available Seats</h1>}
                {this.state.seat.map((seat) => {
                  let count = 0
                  let button = 0
                  {this.state.reserved.map((reserved) => {
                    if(seat === reserved) {
                      count = count+1
                    }
                  })}
                  {this.state.id.map((booked) => {
                    if(booked == seat) {
                      button = button + 1
                    }
                  })}
                  if(button !== 0 ) {
                      return (
                        <Button style={{color: 'white', backgroundColor:'blue', marginRight: 10, marginBottom: 40,cursor: 'not-allowed'}}>{seat}</Button>
                      )
                  }
                  if(count == 0) {
                    return(
                      <Button style={{color: 'white', backgroundColor:'green' , marginRight: 10, marginBottom: 40}} onClick={() =>{this.button(seat)}}>{seat}</Button>
                      )
                  } else {
                    return (
                      <Button style={{color: 'white', backgroundColor:'red', marginRight: 10, marginBottom: 40,cursor: 'not-allowed'}}>{seat}</Button>
                    )
                  }
                })}
                </Grid>
                <hr/>
                <Grid item>
                      <Button style={{color: 'white', backgroundColor:'blue', marginRight: 10, marginTop: 40}} onClick={() => this.book(theatreId, movieId)}>Book</Button>
                </Grid>
                <Grid item>
                  {localStorage.getItem('role') == 'owner' ? <Button style={{color: 'white', backgroundColor:'blue', marginRight: 10, marginTop: 40}} 
                  onClick={() => this.reset(theatreId, movieId)}>Reset Seats</Button> : ''}
                </Grid>
            </div>
            </Grid>
        )
    }
}



export default withRouter(SeatList)