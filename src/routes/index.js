import React, {Component} from 'react'
// import history from '../history';
import { Router,  } from 'react-router';
import { Switch, Route, withRouter } from "react-router-dom";
import Login from '../components/Login/login'
import Signup from '../components/Signup/Signup'
import MovieList from '../components/MovieList/movieList'
import SeatList from '../components/Seats/seatList'
import ListTheatre from '../components/Theatre/listTheatre'
import UnAuthorized from '../components/UnAuthorized/unAuthorized'
import addMovies from '../components/MovieList/addMovies'
import addTheatre from '../components/MovieList/addTheatre'
import Tickets from '../components/Tickets/tickets'
import MovieTheatre from '../components/MovieList/movieTheatre';
import ForgetPassword from '../components/ForgetPassword/forgetPassword'

class Routes extends Component {
    render() {
        return (
            <Router history={this.props.history}>
                    <Route exact path='/' exact component={Login} />
                    <Route path='/signup' exact component={Signup} />
                    <Route exact path='/movies' component={MovieList}/>
                    <Route exact path='/seat' component = {SeatList} />
                    <Route exact path = '/theatre' component = {ListTheatre} />
                    <Route exact path = '/unauthorization' component = {UnAuthorized} />
                    <Route exact path = '/addMovies' component = {addMovies} />
                    <Route exact path = '/addTheatre' component = {addTheatre} />
                    <Route exact path = '/tickets' component = {Tickets} />
                    <Route exact path = '/movie/theatre' component = {MovieTheatre} />
                    <Route exact path = '/forget/password' component = {ForgetPassword} />
            </Router>
        )
    }
}

export default withRouter(Routes)